/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac;

import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class QuandoBuscarNoGooglePorSenac {

    public static void main(String... a) {

        WebDriver firefoxDriver = new FirefoxDriver();

        //Open the url which we want in firefox
        firefoxDriver.get("https://www.google.com");

        firefoxDriver.manage().window().maximize();

        WebElement element = firefoxDriver.findElement(By.name("q"));
        element.sendKeys("Cheese!\n");
        element.submit();

        //Wait until the google page shows the result
        WebElement myDynamicElement = (new WebDriverWait(firefoxDriver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("resultStats")));

        List<WebElement> findElements = firefoxDriver.findElements(By.xpath("//*[@id='rso']//h3/a"));

        //Get the url of third link and navigate to it
        String third_link = findElements.get(2).getAttribute("href");
        firefoxDriver.navigate().to(third_link);

    }

}
